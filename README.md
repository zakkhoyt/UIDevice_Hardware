## UIDevice+Hardware.swift

An extension on UIDevice for retrieving the device hardware model.

 
.appleHardware -> Returns an instance of AppleHardware (String enum). For example:

````
case iPhone8_1 = "iPhone8,1"
````

.hardware -> Returns an instance of Hardware (also a string enum) of a more human readable form. For example:

````
case iPhone_8 = "iPhone 8"
````

#### Example Usage

````
  let model = UIDevice.current.model
  print("model: " + model)

  let modelName = UIDevice.current.modelName
  print("modelName: " + modelName)

  let appleHardware = UIDevice.current.appleHardware
  print("appleHardware: " + appleHardware.rawValue)

  do {
      let hardware = appleHardware.hardware
      print("hardware: " + hardware.rawValue)
  }

  do {
      let hardware = UIDevice.current.hardware
      print("hardware: " + hardware.rawValue)
  }
````
