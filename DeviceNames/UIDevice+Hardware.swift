//
//  AppleDeviceName.swift
//  DeviceNames
//
//  Created by Zakk Hoyt on 11/14/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//
//  Provides a human relatable hardware name

import UIKit

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    
    var appleHardware: AppleHardware {
        let modelName = UIDevice.current.modelName
        if let device = AppleHardware(rawValue: modelName) {
            return device
        } else {
            return AppleHardware.unknown
        }
    }

    var hardware: Hardware {
        let modelName = UIDevice.current.modelName
        if let device = AppleHardware(rawValue: modelName) {
            return device.hardware
        } else {
            return AppleHardware.unknown.hardware
        }
    }
}

// Human readable hardware types
enum Hardware: String {
    case unknown = "Unknown"
    case iPodTouch_5 = "iPod Touch 5"
    case iPodTouch_6 =  "iPod Touch 6"
    case iPhone_4 =  "iPhone 4"
    case iPhone_4s =  "iPhone 4s"
    case iPhone_5 = "iPhone 5"
    case iPhone_5c = "iPhone 5c"
    case iPhone_5s = "iPhone 5s"
    case iPhone_6 = "iPhone 6"
    case iPhone_6plus = "iPhone 6 Plus"
    case iPhone_6s = "iPhone 6s"
    case iPhone_6splus = "iPhone 6s Plus"
    case iPhone_7 = "iPhone 7"
    case iPhone_7plus = "iPhone 7 Plus"
    case iPhone_se = "iPhone SE"
    case iPhone_8 = "iPhone 8"
    case iPhone_8plus = "iPhone 8 Plus"
    case iPhone_x = "iPhone X"
    case iPad_2 = "iPad 2"
    case iPad_3 = "iPad 3"
    case iPad_4 = "iPad 4"
    case iPad_air = "iPad Air"
    case iPad_air_2 = "iPad Air 2"
    case iPad_5 = "iPad 5"
    case iPad_mini = "iPad Mini"
    case iPad_mini_2 = "iPad Mini 2"
    case iPad_mini_3 = "iPad Mini 3"
    case iPad_mini_4 = "iPad Mini 4"
    case iPad_pro_9_7 = "iPad Pro 9.7 Inch"
    case iPad_pro_12_9 = "iPad Pro 12.9 Inch"
    case iPad_pro_12_9_v2 = "iPad Pro 12.9 Inch 2. Generation"
    case iPad_pro_10_5 = "iPad Pro 10.5 Inch"
    case appleTV = "Apple TV"
    case appleTV_4k = "Apple TV 4K"
    case homePod = "HomePod"
    case simulator = "Simulator"
}

// Store Apple's hardware schema in an enum
enum AppleHardware: String {
    case unknown = "Unknown"
    case iPod5_1 = "iPod5,1"
    case iPod7_1 = "iPod7,1"
    case iPhone3_1 = "iPhone3,1"
    case iPhone3_2 = "iPhone3,2"
    case iPhone3_3 = "iPhone3,3"
    case iPhone4_1 = "iPhone4,1"
    case iPhone5_1 = "iPhone5,1"
    case iPhone5_2 = "iPhone5,2"
    case iPhone5_3 = "iPhone5,3"
    case iPhone5_4 = "iPhone5,4"
    case iPhone6_1 = "iPhone6,1"
    case iPhone6_2 = "iPhone6,2"
    case iPhone7_2 = "iPhone7,2"
    case iPhone7_1 = "iPhone7,1"
    case iPhone8_1 = "iPhone8,1"
    case iPhone8_2 = "iPhone8,2"
    case iPhone9_1 = "iPhone9,1"
    case iPhone9_3 = "iPhone9,3"
    case iPhone9_2 = "iPhone9,2"
    case iPhone9_4 = "iPhone9,4"
    case iPhone8_4 = "iPhone8,4"
    case iPhone10_1 = "iPhone10,1"
    case iPhone10_4 = "iPhone10,4"
    case iPhone10_2 = "iPhone10,2"
    case iPhone10_5 = "iPhone10,5"
    case iPhone10_3 = "iPhone10,3"
    case iPhone10_6 = "iPhone10,6"
    case iPad2_1 = "iPad2,1"
    case iPad2_2 = "iPad2,2"
    case iPad2_3 = "iPad2,3"
    case iPad2_4 = "iPad2,4"
    case iPad3_1 = "iPad3,1"
    case iPad3_2 = "iPad3,2"
    case iPad3_3 = "iPad3,3"
    case iPad3_4 = "iPad3,4"
    case iPad3_5 = "iPad3,5"
    case iPad3_6 = "iPad3,6"
    case iPad4_1 = "iPad4,1"
    case iPad4_2 = "iPad4,2"
    case iPad4_3 = "iPad4,3"
    case iPad5_3 = "iPad5,3"
    case iPad5_4 = "iPad5,4"
    case iPad6_11 = "iPad6,11"
    case iPad6_12 = "iPad6,12"
    case iPad2_5 = "iPad2,5"
    case iPad2_6 = "iPad2,6"
    case iPad2_7 = "iPad2,7"
    case iPad4_4 = "iPad4,4"
    case iPad4_5 = "iPad4,5"
    case iPad4_6 = "iPad4,6"
    case iPad4_7 = "iPad4,7"
    case iPad4_8 = "iPad4,8"
    case iPad4_9 = "iPad4,9"
    case iPad5_1 = "iPad5,1"
    case iPad5_2 = "iPad5,2"
    case iPad6_3 = "iPad6,3"
    case iPad6_4 = "iPad6,4"
    case iPad6_7 = "iPad6,7"
    case iPad6_8 = "iPad6,8"
    case iPad7_1 = "iPad7,1"
    case iPad7_2 = "iPad7,2"
    case iPad7_3 = "iPad7,3"
    case iPad7_4 = "iPad7,4"
    case AppleTV5_3 = "AppleTV5,3"
    case AppleTV6_2 = "AppleTV6,2"
    case AudioAccessory1_1 = "AudioAccessory1,1"
    case i386 = "i386"
    case x86_6 = "x86_64"
    case simulator = "Simulator"
    
    var hardware: Hardware {
        switch self {
        case .unknown:
            return Hardware.unknown
        case .iPod5_1:
            return Hardware.iPodTouch_5
        case .iPod7_1:
            return Hardware.iPodTouch_6
        case .iPhone3_1, .iPhone3_2, .iPhone3_3:
            return Hardware.iPhone_4
        case .iPhone4_1:
            return Hardware.iPhone_4s
        case .iPhone5_1, .iPhone5_2:
            return Hardware.iPhone_5
        case .iPhone5_3, .iPhone5_4:
            return Hardware.iPhone_5c
        case .iPhone6_1, .iPhone6_2:
            return Hardware.iPhone_5s
        case .iPhone7_2:
            return Hardware.iPhone_6
        case .iPhone7_1:
            return Hardware.iPhone_6plus
        case .iPhone8_1:
            return Hardware.iPhone_6s
        case .iPhone8_2:
            return Hardware.iPhone_6splus
        case .iPhone9_1, .iPhone9_3:
            return Hardware.iPhone_7
        case .iPhone9_2, .iPhone9_4:
            return Hardware.iPhone_7plus
        case .iPhone8_4:
            return Hardware.iPhone_se
        case .iPhone10_1, .iPhone10_4:
            return Hardware.iPhone_8
        case .iPhone10_2, .iPhone10_5:
            return Hardware.iPhone_8plus
        case .iPhone10_3, .iPhone10_6:
            return Hardware.iPhone_x
        case .iPad2_1, .iPad2_2, .iPad2_3, .iPad2_4:
            return Hardware.iPad_2
        case .iPad3_1, .iPad3_2, .iPad3_3:
            return Hardware.iPad_3
        case .iPad3_4, .iPad3_5, .iPad3_6:
            return Hardware.iPad_4
        case .iPad4_1, .iPad4_2, .iPad4_3:
            return Hardware.iPad_air
        case .iPad5_3, .iPad5_4:
            return Hardware.iPad_air_2
        case .iPad6_11, .iPad6_12:
            return Hardware.iPad_5
        case .iPad2_5, .iPad2_6, .iPad2_7:
            return Hardware.iPad_mini
        case .iPad4_4, .iPad4_5, .iPad4_6:
            return Hardware.iPad_mini_2
        case .iPad4_7, .iPad4_8, .iPad4_9:
            return Hardware.iPad_mini_3
        case .iPad5_1, .iPad5_2:
            return Hardware.iPad_mini_4
        case .iPad6_3, .iPad6_4:
            return Hardware.iPad_pro_9_7
        case .iPad6_7, .iPad6_8:
            return Hardware.iPad_pro_12_9
        case .iPad7_1, .iPad7_2:
            return Hardware.iPad_pro_12_9_v2
        case .iPad7_3, .iPad7_4:
            return Hardware.iPad_pro_10_5
        case .AppleTV5_3:
            return Hardware.appleTV
        case .AppleTV6_2:
            return Hardware.appleTV_4k
        case .AudioAccessory1_1:
            return Hardware.homePod
        case .i386, .x86_6:
            return Hardware.simulator
        default:
            return Hardware.unknown
        }
    }
}


